#pragma once

#include <limits>

#include "core/types/fwd.hh"

namespace as
{
    const std::string unit = "m";
    constexpr FloatScalar m = 1.0; // meters

    constexpr FloatScalar mm = 1.0e-3 * m; // millimeters
    constexpr FloatScalar cm = 1.0e-2 * m; // centimeters
    constexpr FloatScalar km = 1.0e3 * m; // kilometers

    constexpr FloatScalar RAYLEIGH_H0 = 7.994 * km;
    constexpr FloatScalar MIE_H0 = 1.200 * km;

    const Spectrum RAYLEIGH_BETA{3.8e-6, 13.5e-6, 33.1e-6};
    const Spectrum MIE_BETA{21.0e-6, 21.0e-6, 21.0e-6};

    constexpr FloatScalar INTERSECTION_BIAS = 1.0 * cm;
    constexpr FloatScalar EPSILON = std::numeric_limits<FloatScalar>::epsilon();

} // namespace as
