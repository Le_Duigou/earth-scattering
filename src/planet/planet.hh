#pragma once

#include <cstddef>
#include <memory>
#include <optional>
#include <vector>

#include "core/intersection/intersection.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"

namespace as
{
    class Planet
    {
    public:
        Planet(FloatVector location, FloatScalar radius, FloatScalar atmosphere_height,
               size_t atmosphere_layer_count, Spectrum color);

        static std::shared_ptr<Planet> make_earth(size_t atmosphere_layer_count);

        FloatVector location() const
        {
            return _location;
        }

        FloatScalar radius() const
        {
            return _radius;
        }

        FloatScalar atmosphere_height() const
        {
            return _atmosphere_height;
        }

        FloatScalar atmosphere_radius() const
        {
            return radius() + atmosphere_height();
        }

        /// Simple brdf (constant diffuse value)
        Spectrum brdf() const;

        /// Get the distance of a point to the ground sphere.
        FloatScalar distance_to_ground(const FloatVector& point) const;

        /// Cast a ray to the ground and get the intersection point
        std::optional<Intersection> intersect_ray_with_ground(const FloatRay& ray) const;

        /// Cast a ray to the outer atmosphere and get the intersection point
        std::optional<Intersection> intersect_ray_with_outer_atmosphere(const FloatRay& ray) const;

        /// Cast a ray to all the atmosphere layers and get the intersection point of the closest
        /// one
        std::optional<Intersection> intersect_ray_with_atmosphere_layers(const FloatRay& ray) const;

        /// Whether or not a ray is shadowed by this planet. Rays casted on the ground surface or
        /// inside it are considered shadowed
        bool is_ray_shadowed(const FloatRay& ray) const;

    private:
        void compute_atmosphere_layers_radius(size_t layer_count);
        FloatScalar get_atmosphere_layer_radius(size_t layer_id, size_t layer_count) const;

    private:
        FloatVector _location;
        FloatScalar _radius;
        FloatScalar _atmosphere_height;
        std::vector<FloatScalar> _atmosphere_layers_radius;
        Spectrum _constant_color;
    };
} // namespace as
