#include "planet.hh"

#include <array>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <memory>

#include "constants/constants.hh"
#include "core/intersection/intersection.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"

namespace as
{
    std::shared_ptr<Planet> Planet::make_earth(size_t atmosphere_layer_count)
    {
        return std::make_shared<Planet>(FloatVector{0.0, 0.0, 0.0},
                                        6360 * km, // 6 360 km
                                        60 * km, // 60 km
                                        atmosphere_layer_count, // Layer count
                                        as::Spectrum{0.2, 0.2, 0.2} // Color
        );
    }

    Planet::Planet(FloatVector location, FloatScalar radius, FloatScalar atmosphere_height,
                   size_t layer_count, Spectrum color)
        : _location(location)
        , _radius(radius)
        , _atmosphere_height(atmosphere_height)
        , _constant_color(color)
    {
        compute_atmosphere_layers_radius(layer_count);

#ifndef NDEBUG
        std::cout << "Ground radius: " << radius << unit << "\n";
        std::cout << "Atmosphere radius: " << atmosphere_radius() << unit << "\n";

        size_t i = 0;
        for (const auto& layer_radius : _atmosphere_layers_radius)
        {
            std::cout << "Layer " << i << ": " << layer_radius << unit << "\n";
            if (layer_radius > atmosphere_radius())
                std::cerr << "Layer " << i << " has a too big radius\n";
            i++;
        }
#endif
    }

    void Planet::compute_atmosphere_layers_radius(size_t layer_count)
    {
        // We generate the layers
        _atmosphere_layers_radius.emplace_back(radius());
        for (size_t i = 0; i < layer_count; i++)
        {
            const FloatScalar layer_radius = get_atmosphere_layer_radius(i, layer_count);
            if (layer_radius > _radius && layer_radius < atmosphere_radius())
                _atmosphere_layers_radius.emplace_back(layer_radius);
        }
        _atmosphere_layers_radius.emplace_back(atmosphere_radius());
    }

    FloatScalar Planet::get_atmosphere_layer_radius(size_t layer_id, size_t layer_count) const
    {
        const FloatScalar rho_i =
            1.0 - (static_cast<FloatScalar>(layer_id) / static_cast<FloatScalar>(layer_count));
        return -RAYLEIGH_H0 * std::log(rho_i) + radius();
    }

    FloatScalar Planet::distance_to_ground(const FloatVector& point) const
    {
        return (point - _location).norm() - radius();
    }

    Spectrum Planet::brdf() const
    {
        const Spectrum diffuse = _constant_color / M_PI;

        return diffuse;
    }

    std::optional<Intersection> Planet::intersect_ray_with_ground(const FloatRay& ray) const
    {
        const auto [d1, d2] = ray.intersection_with_sphere(location(), radius());

        if (!d1)
            return {};
        else
            return Intersection{
                ray.point_at(*d1),
                *d1,
                ray.point_at(d2.has_value() ? *d2 : *d1),
            };
    }

    std::optional<Intersection>
    Planet::intersect_ray_with_outer_atmosphere(const FloatRay& ray) const
    {
        const auto [d1, d2] = ray.intersection_with_sphere(location(), atmosphere_radius());

        if (!d1)
            return {};
        else
            return Intersection{
                ray.point_at(*d1),
                *d1,
                ray.point_at(d2.has_value() ? *d2 : *d1),
            };
    }

    std::optional<Intersection>
    Planet::intersect_ray_with_atmosphere_layers(const FloatRay& ray) const
    {
#ifdef NDEBUG
        if (_atmosphere_layers_radius.size() < 2)
        {
            std::cerr << "Error: The rendered planet have less than 2 atmosphere layers\n";
            throw std::logic_error("Invalid planet");
        }
#endif
        // Instead of computing the intersection with every layer, we only check the closest.
        //
        // Indeed, since all the sphere are centered around the same point, a ray has to cross them
        // in order. We thus avoid computing expensive collisions.
        const double origin_distance = (ray.origin - location()).norm();

        std::array<double, 3> radius_to_test{0.0, 0.0, 0.0};
        if (origin_distance > _atmosphere_layers_radius.back())
        {
            radius_to_test[0] = _atmosphere_layers_radius.back();
        }
        else
        {
            radius_to_test[0] = 0;
            radius_to_test[1] = _atmosphere_layers_radius[0];

            for (size_t i = 1; i < _atmosphere_layers_radius.size(); i++)
            {
                radius_to_test[0] = radius_to_test[1];
                radius_to_test[1] = _atmosphere_layers_radius[i];

                // Tricky case: We are exactly on one layer: we should check three layers
                if (std::abs(origin_distance - radius_to_test[1]) < INTERSECTION_BIAS
                    && i + 1 < _atmosphere_layers_radius.size())
                {
                    radius_to_test[2] = _atmosphere_layers_radius[i + 1];
                    break;
                }

                if (origin_distance < radius_to_test[1])
                    break;
            }
        }

        FloatScalar smallest_depth = INFINITY;
        FloatScalar smallest_depth_back = INFINITY;

        for (FloatScalar layer_radius : radius_to_test)
        {
            const auto [d1, d2] = ray.intersection_with_sphere(location(), layer_radius);

            if (d1 && *d1 < smallest_depth)
            {
                smallest_depth = *d1;
                smallest_depth_back = d2.has_value() ? *d2 : *d1;
            }
        }

        if (!std::isfinite(smallest_depth))
            return {};
        else
            return Intersection{
                ray.point_at(smallest_depth),
                smallest_depth,
                ray.point_at(smallest_depth_back),
            };
    }

    bool Planet::is_ray_shadowed(const FloatRay& ray) const
    {
        return distance_to_ground(ray.origin) < INTERSECTION_BIAS / 2.0
            || intersect_ray_with_ground(ray).has_value();
    }

} // namespace as
