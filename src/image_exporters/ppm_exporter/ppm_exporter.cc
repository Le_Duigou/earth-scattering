#include "ppm_exporter.hh"

#include <algorithm>
#include <fstream>
#include <ios>
#include <iostream>

#include "core/types/rgb_color.hh"
#include "core/types/spectrum.hh"

namespace as
{
    RgbColor<unsigned char> correct_and_quantize_color(const Spectrum& src);

    PpmImageExporter::PpmImageExporter(const std::string& path)
        : _path(path)
    {}

    bool PpmImageExporter::export_image(const image_t& image) const
    {
        constexpr auto flags = std::fstream::binary | std::fstream::trunc | std::fstream::out;
        try
        {
            auto file = std::fstream(_path, flags);

            write_image(image, file);

            std::cout << "Saved image to file: " << _path << '\n';
            return true;
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what();
        }
        return false;
    }

    void PpmImageExporter::write_image(const image_t& image, std::ostream& out) const
    {
        // Header
        const auto shape = image.shape();
        out << "P6 " << shape[0] << ' ' << shape[1] << ' ' << 255 << '\n';

        // Content
        const size_t size = shape[0] * shape[1];
        for (size_t i = 0; i < size; i++)
        {
            const RgbColor<unsigned char> color = correct_and_quantize_color(*image.get(i));

            out << color.r << color.g << color.b;
        }
        out << '\n';
    }

    double correct_color_component(double comp);

    RgbColor<unsigned char> correct_and_quantize_color(const Spectrum& src)
    {
        return {
            static_cast<unsigned char>(
                std::clamp(correct_color_component(src.r) * 255, 0.0, 255.0)),
            static_cast<unsigned char>(
                std::clamp(correct_color_component(src.g) * 255, 0.0, 255.0)),
            static_cast<unsigned char>(
                std::clamp(correct_color_component(src.b) * 255, 0.0, 255.0)),
        };
    }

    double correct_color_component(double comp)
    {
        return comp <= 0.0031308 ? comp * 12.92 : std::pow(comp, 1.0 / 2.4) * 1.055 - 0.055;
    }
} // namespace as
