#pragma once

#include <string>

#include "core/image/image_exporter.hh"
#include "core/types/spectrum.hh"

namespace as
{
    class PpmImageExporter : public ImageExporter<Spectrum, 2>
    {
    public:
        PpmImageExporter(const std::string& path);

        bool export_image(const image_t& image) const override;

    private:
        void write_image(const image_t& image, std::ostream& out) const;

    private:
        std::string _path;
    };
} // namespace as
