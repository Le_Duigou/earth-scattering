#include "scene.hh"

namespace as
{
    Scene::Scene(FloatVector sun_direction, Spectrum sun_irradiance, std::shared_ptr<Planet> planet,
                 std::shared_ptr<Camera> camera)
        : _sun_direction(sun_direction.normalize())
        , _sun_irradiance(sun_irradiance)
        , _planet(planet)
        , _camera(camera)
    {}
} // namespace as
