#pragma once

#include <memory>

#include "camera/camera.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"
#include "planet/planet.hh"

namespace as
{
    class Scene
    {
    public:
        Scene(FloatVector sun_direction, Spectrum sun_irradiance, std::shared_ptr<Planet> planet,
              std::shared_ptr<Camera> camera);

        const FloatVector& sun_direction() const
        {
            return _sun_direction;
        }
        const Spectrum& sun_irradiance() const
        {
            return _sun_irradiance;
        }

        std::shared_ptr<Planet> planet() const
        {
            return _planet;
        }

        std::shared_ptr<Camera> camera() const
        {
            return _camera;
        }

    private:
        FloatVector _sun_direction;
        Spectrum _sun_irradiance;
        std::shared_ptr<Planet> _planet;
        std::shared_ptr<Camera> _camera;
    };
} // namespace as
