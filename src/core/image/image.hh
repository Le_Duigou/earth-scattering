#pragma once

#include <array>
#include <cstddef>
#include <iostream>
#include <numeric>
#include <optional>
#include <vector>

namespace as
{
    /// A generic ``N`` dimensional image of pixel of type ``T``.
    template <typename T, unsigned int N>
    class Image
    {
    public:
        static constexpr unsigned int dimension = N;

        using pixel_t = T;
        using container_t = std::vector<pixel_t>;
        using coordinate_t = std::array<size_t, dimension>;

    public:
        /// Create a new image with the given shape. Axis with a size of 0 are
        /// not allowed.
        Image(coordinate_t shape, const pixel_t& value)
            : _content(container_t(
                std::accumulate(shape.begin(), shape.end(), 1, std::multiplies<unsigned int>()),
                value))
            , _shape(shape)
        {}

        /// Get the shape of the image (number of pixel in each dimension)
        const coordinate_t& shape() const
        {
            return _shape;
        }

        /// Get the total number of pixel of the image
        size_t size() const
        {
            return _content.size();
        }

        /// Get the value of a pixel or nullopt if out of bounds
        std::optional<pixel_t> get(size_t index) const
        {
            if (index >= _content.size())
                return std::nullopt;
            return _content[index];
        }

        /// Get the value of a pixel or nullopt if out of bounds
        std::optional<pixel_t> get(coordinate_t coordinate) const
        {
            const std::optional<size_t> index = coordinate_to_index(coordinate);
            if (!index)
                return std::nullopt;
            return _content[*index];
        }

        /// Set the value of a pixel
        bool set(size_t index, pixel_t value)
        {
            if (index >= _content.size())
                return false;

            _content[index] = value;
            return true;
        }

        /// Set the value of a pixel
        bool set(coordinate_t coordinate, pixel_t value)
        {
            const std::optional<size_t> index = coordinate_to_index(coordinate);
            if (!index)
                return false;

            _content[*index] = value;
            return true;
        }

        /// Return the aspect ratio of the image (defined only for 2D images)
        double aspect_ratio() const
        {
            if constexpr (dimension == 2)
            {
                return static_cast<double>(_shape[0]) / static_cast<double>(_shape[1]);
            }
            else
            {
                return 0.0;
            }
        }

        /// Convert the index of a pixel to its coordinates
        std::optional<size_t> coordinate_to_index(const coordinate_t& coordinate) const
        {
            if constexpr (dimension == 2)
            {
                return coordinate[0] + coordinate[1] * _shape[0];
            }
            else
            {
                size_t c = _content.size();
                size_t value = 0;

                for (size_t i = 0; i < dimension; i++)
                {
                    const size_t j = dimension - 1 - i;
                    c /= _shape[j];

                    if (coordinate[j] >= _shape[j])
                        return std::nullopt;

                    value += c * coordinate[j];
                }

                return value;
            }
        }

        /// Convert the coordinates of a pixel to its index
        coordinate_t index_to_coordinate(size_t index) const
        {
            if constexpr (dimension == 2)
            {
                return {index % _shape[0], index / _shape[0]};
            }
            else
            {
                size_t c = _content.size();
                coordinate_t value = 0;

                if (index >= c)
                    return std::nullopt;
                for (size_t i = 0; i < dimension; i++)
                {
                    const size_t j = dimension - 1 - i;
                    c /= _shape[j];
                    const size_t tmp = index / c;
                    index -= tmp * c;

                    value[j] = c * tmp;
                }

                return value;
            }
        }

    private:
        std::vector<pixel_t> _content;
        const coordinate_t _shape;
    };
} // namespace as
