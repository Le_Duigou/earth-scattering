#pragma once

#include "image.hh"

namespace as
{
    /// An interface to export ``Image``s.
    template <typename T, unsigned int D>
    class ImageExporter
    {
    public:
        using image_t = Image<T, D>;

    public:
        /// Export an image and return ``true`` on success.
        virtual bool export_image(const image_t& image) const = 0;
    };
} // namespace as
