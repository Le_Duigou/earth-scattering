#pragma once

#include "core/types/fwd.hh"
namespace as
{
    /// A structured representation of the intersection between a ray and an entity
    struct Intersection
    {
        FloatVector location; ///< Location of the intersection
        FloatScalar depth; ///< Depth of the ray before the first intersection
        FloatVector back_location; ///< Location of the intersection where the ray goes out
    };
} // namespace as
