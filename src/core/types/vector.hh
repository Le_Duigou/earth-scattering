#pragma once

#include <cmath>
#include <ostream>

namespace as
{
    /// A 3D vector
    template <typename T>
    class Vector
    {
    public:
        using scalar_t = T;
        using self_t = Vector<scalar_t>;

        scalar_t x = 0;
        scalar_t y = 0;
        scalar_t z = 0;

        inline bool operator==(const self_t& other) const
        {
            return other.x == x && other.y == y && other.z == z;
        }

        inline bool operator!=(const self_t& other) const
        {
            return !((*this) == other);
        }

        inline self_t operator+(const self_t& other) const
        {
            return self_t{x + other.x, y + other.y, z + other.z};
        }

        inline self_t operator-(const self_t& other) const
        {
            return self_t{x - other.x, y - other.y, z - other.z};
        }

        inline self_t operator-() const
        {
            return self_t{-x, -y, -z};
        }

        inline self_t& operator+=(const self_t& other)
        {
            x += other.x;
            y += other.y;
            z += other.z;
            return *this;
        }

        inline self_t& operator-=(const self_t& other)
        {
            x -= other.x;
            y -= other.y;
            z -= other.z;
            return *this;
        }

        inline self_t operator*(scalar_t scalar) const
        {
            return self_t{static_cast<scalar_t>(x * scalar), static_cast<scalar_t>(y * scalar),
                          static_cast<scalar_t>(z * scalar)};
        }

        inline self_t operator/(scalar_t scalar) const
        {
            return self_t{static_cast<scalar_t>(x / scalar), static_cast<scalar_t>(y / scalar),
                          static_cast<scalar_t>(z / scalar)};
        }

        inline self_t operator*(const self_t& other) const
        {
            return self_t{x * other.x, y * other.y, z * other.z};
        }

        inline self_t operator/(const self_t& other) const
        {
            return self_t{x / other.x, y / other.y, z / other.z};
        }

        inline scalar_t dot(const self_t& other) const
        {
            return x * other.x + y * other.y + z * other.z;
        }

        inline self_t cross(const self_t& other) const
        {
            return self_t{
                (y * other.z) - (z * other.y),
                (z * other.x) - (x * other.z),
                (x * other.y) - (y * other.x),
            };
        }

        inline bool is_null() const
        {
            return x == 0 && y == 0 && z == 0;
        }

        inline scalar_t squared_norm() const
        {
            return x * x + y * y + z * z;
        }

        inline scalar_t norm() const
        {
            return std::sqrt(squared_norm());
        }

        inline self_t normalize() const
        {
            return is_null() ? Vector{*this} : (*this) / norm();
        }

        inline scalar_t angle_to(const self_t& other) const
        {
            return std::acos(dot(other) / (norm() * other.norm()));
        }

        template <typename U>
        inline Vector<U> cast_to() const
        {
            return Vector<U>{
                static_cast<U>(x),
                static_cast<U>(y),
                static_cast<U>(z),
            };
        }
    };

    template <typename T>
    inline std::ostream& operator<<(std::ostream& out, const Vector<T>& vector)
    {
        out << '(' << vector.x << ", " << vector.y << ", " << vector.z << ')';

        return out;
    }
} // namespace as
