#pragma once

#include "ray.hh"
#include "rgb_color.hh"
#include "spectrum.hh"
#include "vector.hh"
#include "vector2.hh"

namespace as
{
    using FloatScalar = double;
    using FloatVector = Vector<FloatScalar>;
    using FloatVector2 = Vector2<FloatScalar>;
    using FloatRay = Ray<FloatScalar>;

    using ImageVector2 = Vector2<size_t>;
} // namespace as
