#pragma once

#include <cmath>
#include <iostream>
#include <limits>
#include <optional>
#include <ostream>
#include <stdexcept>

#include "core/types/vector.hh"

namespace as
{
    template <typename T>
    class Ray
    {
    public:
        using scalar_t = T;
        using vector_t = Vector<scalar_t>;
        using self_t = Ray<scalar_t>;

    public:
        vector_t origin;
        vector_t direction;
        scalar_t limit;

    public:
        Ray(vector_t origin, vector_t direction)
            : Ray(origin, direction, INFINITY)
        {}

        Ray(vector_t origin, vector_t direction, scalar_t limit)
            : origin(origin)
            , direction(direction.normalize())
            , limit(limit)
        {
#ifndef NDEBUG
            if (direction.is_null())
            {
                std::cerr << "Warning: The direction of a ray is null.\n";
            }

            if (limit <= 0.0)
            {
                std::cerr << "Warning: The limit of a ray is 0 or negative.\n";
            }
#endif
        }

        /// Get a ray from point_1 to point_2
        static self_t get_between(vector_t point_1, vector_t point_2)
        {
            const vector_t difference = point_2 - point_1;
            const scalar_t distance = difference.norm();
            return self_t{point_1, difference / distance, distance};
        }

        /// Get the location of a point at a distance on the ray
        vector_t point_at(scalar_t distance) const
        {
            return origin + direction * distance;
        }

        /// Check an intersection with a sphere and return the distances of
        /// all intersection points (max 2)
        std::pair<std::optional<scalar_t>, std::optional<scalar_t>>
        intersection_with_sphere(vector_t center, scalar_t radius) const
        {
#ifndef NDEBUG
            if (!center.is_null())
                std::cerr << "Warning: Non-nul center sphere intersection are not supported yet.\n";
                // TODO: handle them
#endif
            // Put everything in double to handle better precision errors

            const vector_t to_center = origin - center;
            const scalar_t dot = direction.dot(to_center);
            const scalar_t center_distance = to_center.norm();

            // Quadratic equation solving
            const double a = direction.squared_norm();
            const double b = dot;
            const double c = (center_distance * center_distance) - (radius * radius);

            const double delta = (b * b) - a * c;

            if (delta < 0.0) // Case 1: No collision
                return std::make_pair(std::nullopt, std::nullopt);

            if (delta == 0) // Case 2: One collision point (rare)
            {
                const double d1 = -b;
                return (d1 > 0 && d1 <= limit)
                    ? std::make_pair(std::make_optional(d1), std::nullopt)
                    : std::make_pair(std::nullopt, std::nullopt);
            }

            // Case 3: Two collision points
            const double delta_sqrt = std::sqrt(delta);

            const double d1 = -b - delta_sqrt;
            const double d2 = -b + delta_sqrt;

            const bool is_d1_valid = d1 > 0 && d1 <= limit;
            const bool is_d2_valid = d2 > 0 && d2 <= limit;

            if (is_d1_valid && is_d2_valid)
                return std::make_pair(std::make_optional(d1), std::make_optional(d2));
            if (is_d1_valid && !is_d2_valid)
                return std::make_pair(std::make_optional(d1), std::nullopt);
            if (!is_d1_valid && is_d2_valid)
                return std::make_pair(std::make_optional(d2), std::nullopt);
            return std::make_pair(std::nullopt, std::nullopt);
        }
    };

    template <typename T>
    inline std::ostream& operator<<(std::ostream& out, const Ray<T>& ray)
    {
        out << '[' << ray.origin << " + d x " << ray.direction;

        if (std::isfinite(ray.limit))
            out << " | 0 < d < " << ray.limit;

        out << ']';

        return out;
    }
}; // namespace as
