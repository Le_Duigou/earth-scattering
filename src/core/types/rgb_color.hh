#pragma once
#include <cmath>
#include <ostream>

namespace as
{
    /// A color represented by red/green/blue channels
    template <typename T>
    class RgbColor
    {
    public:
        using scalar_t = T;
        using self_t = RgbColor<scalar_t>;

        scalar_t r = 0;
        scalar_t g = 0;
        scalar_t b = 0;

        inline bool operator==(const self_t& other) const
        {
            return other.r == r && other.g == g && other.b == b;
        }

        inline bool operator!=(const self_t& other) const
        {
            return !((*this) == other);
        }

        inline self_t operator+(const self_t& other) const
        {
            return self_t{r + other.r, g + other.g, b + other.b};
        }

        inline self_t operator-(const self_t& other) const
        {
            return self_t{r - other.r, g - other.g, b - other.b};
        }

        inline self_t& operator+=(const self_t& other)
        {
            r += other.r;
            g += other.g;
            b += other.b;
            return *this;
        }

        inline self_t& operator-=(const self_t& other)
        {
            r -= other.r;
            g -= other.g;
            b -= other.b;
            return *this;
        }

        inline self_t operator*(scalar_t scalar) const
        {
            return self_t{static_cast<scalar_t>(r * scalar), static_cast<scalar_t>(g * scalar),
                          static_cast<scalar_t>(b * scalar)};
        }

        inline self_t operator/(scalar_t scalar) const
        {
            return self_t{static_cast<scalar_t>(r / scalar), static_cast<scalar_t>(g / scalar),
                          static_cast<scalar_t>(b / scalar)};
        }

        inline self_t operator*(const self_t& other) const
        {
            return self_t{r * other.r, g * other.g, b * other.b};
        }

        inline self_t operator/(const self_t& other) const
        {
            return self_t{r / other.r, g / other.g, b / other.b};
        }

        inline self_t exp() const
        {
            return self_t{std::exp(r), std::exp(g), std::exp(b)};
        }

        inline self_t minus_exp() const
        {
            return self_t{std::exp(-r), std::exp(-g), std::exp(-b)};
        }

    public:
        static self_t black()
        {
            return self_t{0.0, 0.0, 0.0};
        }

        static self_t white()
        {
            return self_t{1.0, 1.0, 1.0};
        }
    };

    template <typename T>
    inline std::ostream& operator<<(std::ostream& out, const RgbColor<T>& color)
    {
        out << "(r: " << color.r << ", g: " << color.g << ", b: " << color.b << ')';

        return out;
    }
} // namespace as
