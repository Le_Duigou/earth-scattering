#pragma once

#include <cmath>
#include <ostream>

namespace as
{
    /// A 2D vector
    template <typename T>
    class Vector2
    {
    public:
        using scalar_t = T;
        using self_t = Vector2<scalar_t>;

        scalar_t x = 0;
        scalar_t y = 0;

        inline bool operator==(const self_t& other) const
        {
            return other.x == x && other.y == y;
        }

        inline bool operator!=(const self_t& other) const
        {
            return !((*this) == other);
        }

        inline self_t operator+(const self_t& other) const
        {
            return self_t{x + other.x, y + other.y};
        }

        inline self_t operator-(const self_t& other) const
        {
            return self_t{x - other.x, y - other.y};
        }

        inline self_t operator-() const
        {
            return self_t{-x, -y};
        }

        inline self_t& operator+=(const self_t& other)
        {
            x += other.x;
            y += other.y;
            return *this;
        }

        inline self_t& operator-=(const self_t& other)
        {
            x -= other.x;
            y -= other.y;
            return *this;
        }

        inline self_t operator*(scalar_t scalar) const
        {
            return self_t{static_cast<scalar_t>(x * scalar), static_cast<scalar_t>(y * scalar)};
        }

        inline self_t operator/(scalar_t scalar) const
        {
            return self_t{static_cast<scalar_t>(x / scalar), static_cast<scalar_t>(y / scalar)};
        }

        inline self_t operator*(const self_t& other) const
        {
            return self_t{x * other.x, y * other.y};
        }

        inline self_t operator/(const self_t& other) const
        {
            return self_t{x / other.x, y / other.y};
        }

        inline scalar_t dot(const self_t& other) const
        {
            return x * other.x + y * other.y;
        }

        inline bool is_null() const
        {
            return x == 0 && y == 0;
        }

        inline scalar_t squared_norm() const
        {
            return x * x + y * y;
        }

        inline scalar_t norm() const
        {
            return std::sqrt(squared_norm());
        }

        inline self_t normalize() const
        {
            return is_null() ? self_t{*this} : (*this) / norm();
        }

        template <typename U>
        inline Vector2<U> cast_to() const
        {
            return Vector2<U>{
                static_cast<U>(x),
                static_cast<U>(y),
            };
        }
    };

    template <typename T>
    inline std::ostream& operator<<(std::ostream& out, const Vector2<T>& vector)
    {
        out << '(' << vector.x << ", " << vector.y << ')';

        return out;
    }
} // namespace as
