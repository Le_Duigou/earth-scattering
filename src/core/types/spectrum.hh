#pragma once

#include "core/types/rgb_color.hh"

namespace as
{
    // Here, we simply defines a spectrum as a RGB color for simplicity.
    // For more accurate representation, a more complex representation would be
    // needed, but it is clearly enough for this use case.

    /// Representation of a light spectrum.
    using Spectrum = RgbColor<double>;
} // namespace as
