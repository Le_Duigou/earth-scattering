#pragma once

#include "core/types/fwd.hh"
namespace as
{
    /// Simple "pinhole" perspective camera
    class Camera
    {
    public:
        Camera(FloatVector location, FloatVector look_at, FloatVector up, FloatScalar fov,
               FloatScalar aspect_ratio);

        FloatRay generate_ray(const FloatVector2& clip_space_target) const;

        FloatVector clip_space_to_world_space(const FloatVector2& source) const;

    private:
        FloatVector _location;
        FloatVector _clip_plane_origin;
        FloatVector _clip_plane_dimensions;
        FloatVector _up;
        FloatVector _right;
    };
} // namespace as
