#include "camera.hh"

#include "core/types/fwd.hh"

template <typename T>
T deg_to_rad(T deg);

namespace as
{
    Camera::Camera(FloatVector location, FloatVector look_at, FloatVector up, FloatScalar fov,
                   FloatScalar aspect_ratio)
        : _location(location)
    {
        // Set up all the variables used by the camera
        constexpr FloatScalar z_near = 1.0;
        const FloatVector front = (look_at - location).normalize();

        FloatScalar fov_x = aspect_ratio < 1.0 ? fov * aspect_ratio : fov;
        FloatScalar fov_y = aspect_ratio < 1.0 ? fov : fov / aspect_ratio;

        // Up and right vector (which are x and y vector of clip space)
        _up = up;
        _right = front.cross(_up);

        // Clip space definition
        _clip_plane_dimensions = {
            z_near * std::tan(deg_to_rad(fov_x / 2)) * 2,
            z_near * std::tan(deg_to_rad(fov_y / 2)) * 2,
        };
        _clip_plane_origin = location + (front * z_near) - _right * (_clip_plane_dimensions.x / 2.0)
            - _up * (_clip_plane_dimensions.y / 2.0);
    }

    FloatRay Camera::generate_ray(const FloatVector2& clip_space_target) const
    {
        const FloatVector world_space_target = clip_space_to_world_space(clip_space_target);

        return FloatRay{_location, (world_space_target - _location).normalize()};
    }

    FloatVector Camera::clip_space_to_world_space(const FloatVector2& source) const
    {
        return _clip_plane_origin + _right * (source.x * _clip_plane_dimensions.x)
            + _up * ((1.0 - source.y) * _clip_plane_dimensions.y);
    }
} // namespace as

template <typename T>
inline T deg_to_rad(T deg)
{
    return deg * M_PI / 180.0;
}
