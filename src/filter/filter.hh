#pragma once
#include <cmath>

#include "core/types/fwd.hh"
namespace as
{
    /// Simple triangle filter
    class Filter
    {
    public:
        Filter(FloatScalar radius)
            : _radius(radius)
        {}

        // Return the radius of the filter
        FloatScalar radius() const
        {
            return _radius;
        }

        // Get the weight at a location (filter is centered on [0,0] and out of bound values are
        // invalid)
        FloatScalar get_weight(FloatVector2 location) const
        {
            return (std::max<FloatScalar>(_radius - std::abs(location.x), 0.0)
                    * std::max<FloatScalar>(_radius - std::abs(location.y), 0.0))
                / (_radius * _radius);
        }

    private:
        FloatScalar _radius;
    };
} // namespace as
