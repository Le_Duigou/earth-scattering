#include "options.hh"

#include <cstddef>
#include <exception>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "constants/constants.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"
#include "scene/scene.hh"

namespace as
{
    std::string get_usage_string(char* name);
    std::unique_ptr<Scene> scene_from_id(size_t id, size_t atmosphere_layer_count,
                                         double aspect_ratio);

    Options::Options(int argc, char** argv)
    {
        int i = 1; // Skip name

        try
        {
            while (i < argc)
            {
                if (argv[i][0] != '-')
                {
                    std::cerr << "Unknown option " << argv[i][0] << "\n";
                    error = true;
                    break;
                }

                const std::string arg = argv[i];

                if (arg == "--dimensions" || arg == "-d")
                {
                    if (i + 2 >= argc)
                    {
                        std::cerr << "Expecting two arguments to " << arg << "\n";
                        error = true;
                        break;
                    }
                    const size_t w = std::stoul(argv[i + 1]);
                    const size_t h = std::stoul(argv[i + 2]);

                    if (w == 0 || h == 0)
                    {
                        std::cerr << "Film dimensions must be non-nul\n";
                        error = true;
                        break;
                    }

                    film_dimension = {w, h};
                    i += 3;
                }
                else if (arg == "--samples" || arg == "-s")
                {
                    if (i + 1 >= argc)
                    {
                        std::cerr << "Expecting one argument to " << arg << "\n";
                        error = true;
                        break;
                    }
                    const size_t count = std::stoul(argv[i + 1]);

                    if (count == 0)
                    {
                        std::cerr << "Sample count must be non-nul\n";
                        error = true;
                        break;
                    }

                    samples = count;
                    i += 2;
                }
                else if (arg == "--layers" || arg == "-l")
                {
                    if (i + 1 >= argc)
                    {
                        std::cerr << "Expecting one argument to " << arg << "\n";
                        error = true;
                        break;
                    }
                    const size_t count = std::stoul(argv[i + 1]);

                    if (count == 0)
                    {
                        std::cerr << "Layer count must be non-nul\n";
                        error = true;
                        break;
                    }

                    atmosphere_layer_count = count;
                    i += 2;
                }
                else if (arg == "--tile-size" || arg == "-t")
                {
                    if (i + 1 >= argc)
                    {
                        std::cerr << "Expecting one argument to " << arg << "\n";
                        error = true;
                        break;
                    }
                    const size_t size = std::stoul(argv[i + 1]);

                    if (size == 0)
                    {
                        std::cerr << "Tile size must be non-nul\n";
                        error = true;
                        break;
                    }

                    tile_size = size;
                    i += 2;
                }
                else if (arg == "--filter-radius" || arg == "-r")
                {
                    if (i + 1 >= argc)
                    {
                        std::cerr << "Expecting one argument to " << arg << "\n";
                        error = true;
                        break;
                    }
                    const double radius = std::stod(argv[i + 1]);

                    if (radius <= 0)
                    {
                        std::cerr << "Filter radius must be positive and non-nul\n";
                        error = true;
                        break;
                    }

                    filter_radius = radius;
                    i += 2;
                }
                else if (arg == "--scene-id" || arg == "-i")
                {
                    if (i + 1 >= argc)
                    {
                        std::cerr << "Expecting one argument to " << arg << "\n";
                        error = true;
                        break;
                    }
                    const size_t id = std::stoul(argv[i + 1]);

                    scene_id = id;
                    i += 2;
                }
            }
        }
        catch (const std::exception& e)
        {
            std::cerr << "Format error on " << argv[i] << "\n";
            error = true;
        }

        const double aspect_ratio =
            static_cast<double>(film_dimension[0]) / static_cast<double>(film_dimension[1]);
        scene = scene_from_id(scene_id, atmosphere_layer_count, aspect_ratio);

        if (!scene)
        {
            std::cerr << "Invalid scene id " << scene_id << "\n";
            error = true;
        }

        if (error)
            std::cerr << get_usage_string(argv[0]) << "\n";
    }

    std::string get_usage_string(char* name)
    {
        std::stringstream s;
        s << "Usage: " << name << " "
          << "[--dimensions width height] "
          << "[--samples count] "
          << "[--tile-size size] "
          << "[--filter-radius radius] "
          << "[--layers count] "
          << "[--scene-id id]";

        return s.str();
    }

    struct SceneProps
    {
        // Camera data
        FloatVector camera_location;
        FloatVector camera_look_at;
        FloatVector camera_up;
        double camera_fov;

        // Sun data
        FloatVector sun_direction;
        Spectrum sun_irradiance;
    };

    std::unique_ptr<Scene> scene_from_id(size_t id, size_t atmosphere_layer_count,
                                         double aspect_ratio)
    {
        const std::array<SceneProps, 6> props{
            // Scene 0
            SceneProps{
                FloatVector{0.0, -15000 * km, 5000 * km}, // Camera location
                FloatVector{0.0, 0.0, 5000 * km}, // Camera look at
                FloatVector{0.0, 0.0, 1.0}, // Camera up vector
                45.0, // Camera fov

                FloatVector{0.0, 1.0, 0.0}, // Sun direction
                Spectrum{1.8, 2.0, 1.75} * 1.0, // Sun irradiance
            },
            // Scene 1
            SceneProps{
                FloatVector{0.0, -15000 * km, 5000 * km}, // Camera location
                FloatVector{0.0, 0.0, 5000 * km}, // Camera look at
                FloatVector{0.0, 0.0, 1.0}, // Camera up vector
                45.0, // Camera fov

                FloatVector{0.0, -1.0, 0.0}, // Sun direction
                Spectrum{1.8, 2.0, 1.75} * 1.0, // Sun irradiance
            },
            // Scene 2
            SceneProps{
                FloatVector{0.0, -15000 * km, 5000 * km}, // Camera location
                FloatVector{0.0, 0.0, 5000 * km}, // Camera look at
                FloatVector{0.0, 0.0, 1.0}, // Camera up vector
                45.0, // Camera fov

                FloatVector{0.0, 0.0, -1.0}, // Sun direction
                Spectrum{1.8, 2.0, 1.75} * 1.0, // Sun irradiance
            },
            // Scene 3
            SceneProps{
                FloatVector{0.0, -15000 * km, 5000 * km}, // Camera location
                FloatVector{0.0, 0.0, 5000 * km}, // Camera look at
                FloatVector{0.0, 0.0, 1.0}, // Camera up vector
                45.0, // Camera fov

                FloatVector{0.5, 0.2, -0.5}, // Sun direction
                Spectrum{1.8, 2.0, 1.75} * 1.0, // Sun irradiance
            },
            // Scene 4
            SceneProps{
                FloatVector{0.0, -15000 * km, 5000 * km}, // Camera location
                FloatVector{0.0, 0.0, 5000 * km}, // Camera look at
                FloatVector{0.0, 0.0, 1.0}, // Camera up vector
                45.0, // Camera fov

                FloatVector{0.0, -1.0, -0.5}, // Sun direction
                Spectrum{1.8, 2.0, 1.75} * 1.0, // Sun irradiance
            },
            // Scene 5
            SceneProps{
                FloatVector{0.0, -20000 * km, 0.0}, // Camera location
                FloatVector{0.0, 0.0, 0.0}, // Camera look at
                FloatVector{0.0, 0.0, 1.0}, // Camera up vector
                45.0, // Camera fov

                FloatVector{1.0, 0.1, -.2}, // Sun direction
                Spectrum{1.8, 2.0, 1.75} * 1.0, // Sun irradiance
            },
        };

        if (id >= props.size())
            return nullptr;

        const SceneProps& scene_props = props[id];

        return std::make_unique<Scene>(
            scene_props.sun_direction, scene_props.sun_irradiance,
            Planet::make_earth(atmosphere_layer_count),
            std::make_shared<Camera>(scene_props.camera_location, scene_props.camera_look_at,
                                     scene_props.camera_up, scene_props.camera_fov, aspect_ratio));
    }
} // namespace as
