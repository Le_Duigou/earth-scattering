#pragma once

#include <array>
#include <cstddef>
#include <memory>

#include "core/types/fwd.hh"
#include "scene/scene.hh"

namespace as
{

    /// Options of the program
    struct Options
    {
    public:
        Options(int argc, char** argv);

        /// Whether or not we have a parsing error
        bool error = false;

        /// Dimensions of the film [--dimensions width height], [-d width height])
        std::array<size_t, 2> film_dimension = {1280, 960};

        /// Sample per pixel ([--samples count], [-s count])
        size_t samples = 4;

        /// Size of a tile ([--tile-size size], [-t size])
        size_t tile_size = 64;

        /// Radius of the filter ([--filter-radius radius], [-r radius])
        double filter_radius = 1.0;

        /// Number of atmosphere layers ([--layers], [-l id])
        size_t atmosphere_layer_count = 6;

        /// Scene id ([--scene-id id], [-i id])
        size_t scene_id = 0;

        /// Scene
        std::unique_ptr<Scene> scene;
    };

} // namespace as
