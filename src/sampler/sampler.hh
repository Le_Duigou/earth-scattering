#pragma once

#include <chrono>
#include <random>
#include <utility>

#include "core/types/vector.hh"
namespace as
{
    /// Simple random sampler
    template <typename T>
    class Sampler
    {
    public:
        using scalar_t = T;

        Sampler()
            : _rng(std::chrono::steady_clock::now().time_since_epoch().count())
        {}

        scalar_t next_1d()
        {
            return static_cast<scalar_t>(_rng()) / static_cast<scalar_t>(_rng.max());
        }

        std::pair<scalar_t, scalar_t> next_2d()
        {
            return {next_1d(), next_1d()};
        }

        Vector<scalar_t> next_3d()
        {
            return {next_1d(), next_1d(), next_1d()};
        }

    private:
        using rng_t = std::mt19937;

        rng_t _rng;
    };
} // namespace as
