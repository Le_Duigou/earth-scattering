#pragma once

#include <functional>
#include <optional>

#include "core/image/image.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"
#include "filter/filter.hh"
#include "sampler/sampler.hh"
#include "scene/scene.hh"

namespace as
{
    /// Main ray tracing integrator
    class Integrator
    {
    public:
        using film_t = Image<Spectrum, 2>;

        Integrator(film_t& film, size_t tile_size, size_t sample, FloatScalar filter_radius);

        void operator()(const Scene& scene);

    private:
        using weights_t = Image<FloatScalar, 2>;

    private:
        void render_tile(const Scene& scene, ImageVector2 tile);

        void add_sample(Spectrum radiance, FloatVector2 sample, FloatVector2 dimensions,
                        ImageVector2 offset, weights_t& weights);
        void scale_samples(ImageVector2 offset, weights_t& weights);

        Spectrum radiance_at(const Scene& scene, const FloatRay& ray);

        // Get the radiance of the ground itself, attenuated by the atmosphere between p_b and p_a.
        // The ground is at location p_b, and p_a is the location where the light enter the
        // atmosphere.
        Spectrum get_attenuated_ground_radiance(const FloatVector& p_a, const FloatVector& p_b,
                                                const Scene& scene);

        // Get the light scattered by the atmosphere between the two points p_a and p_b.
        Spectrum get_atmospheric_scattering(const FloatVector& p_a, const FloatVector& p_b,
                                            const Scene& scene, const FloatRay& view_ray);

        /// Compute the attenuation between two points in the atmosphere and return it as a pair of
        /// Spectrum. The first is for Rayleigh scattering, the second for Mie scattering
        std::pair<Spectrum, Spectrum> t(const FloatVector& p_1, const FloatVector& p_2,
                                        const Scene& scene);

        /// Helper function to integrate between two points by executing the given function each
        /// time an atmospheric layer is hit. It casts rays multiple times from p_1 to p_2 until no
        /// layer is detected.
        ///
        /// Return false if no step has been executed (p_1 and p_2 outside atmosphere or too close,
        /// etc.)
        bool integrate_between(const FloatVector& p_1, const FloatVector& p_2, const Scene& scene,
                               std::function<void(FloatVector p, FloatScalar d)> f) const;

    private:
        film_t& _film;
        size_t _tile_size;
        size_t _sample_per_pixel;
        Sampler<FloatScalar> _sampler;
        Filter _filter;
    };
} // namespace as
