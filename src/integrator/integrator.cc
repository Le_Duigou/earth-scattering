#include "integrator.hh"

#include <algorithm>
#include <array>
#include <assert.h>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <utility>
#include <vector>

#include "constants/constants.hh"
#include "core/image/image.hh"
#include "core/intersection/intersection.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"

namespace as
{

    Integrator::Integrator(film_t& film, size_t tile_size, size_t sample, FloatScalar filter_radius)
        : _film(film)
        , _tile_size(tile_size)
        , _sample_per_pixel(sample)
        , _sampler()
        , _filter(filter_radius)
    {}

    void Integrator::operator()(const Scene& scene)
    {
        const FloatScalar w = _film.shape()[0];
        const FloatScalar h = _film.shape()[1];

        for (size_t tile_x = 0; tile_x * _tile_size < w; tile_x++)
        {
            for (size_t tile_y = 0; tile_y * _tile_size < h; tile_y++)
            {
                render_tile(scene, {tile_x, tile_y});
            }
        }
    }

    void Integrator::render_tile(const Scene& scene, ImageVector2 tile)
    {
        // Coordinate of the first pixel of the tile in the image
        const ImageVector2 offset = tile * _tile_size;
        const FloatVector2 offset_float = offset.cast_to<FloatScalar>();

        // Real dimension of the tile (avoid rendering out of bounds)
        const ImageVector2 dimensions_integer = {
            std::min(_tile_size, _film.shape()[0] - offset.x),
            std::min(_tile_size, _film.shape()[1] - offset.y),
        };
        const FloatVector2 dimensions = dimensions_integer.cast_to<FloatScalar>();
        const FloatVector2 film_dimensions = {static_cast<FloatScalar>(_film.shape()[0]),
                                              static_cast<FloatScalar>(_film.shape()[1])};

        if (dimensions.x == 0 || dimensions.y == 0)
            return;

        // Keep track of the weight of each pixel to correctly scale them back at the end
        weights_t weights{{dimensions_integer.x, dimensions_integer.y}, 0.0};

        using sample_count_t = unsigned long long;
        const sample_count_t sample_count =
            static_cast<sample_count_t>(dimensions_integer.x * dimensions_integer.y)
            * _sample_per_pixel;

        for (sample_count_t i = 0; i < sample_count; i++)
        {
            const auto [x, y] = _sampler.next_2d();
            const FloatVector2 sample{x, y};

            const FloatVector2 clip_space_sample =
                ((sample * dimensions) + offset_float) / film_dimensions;

            const FloatRay ray = scene.camera()->generate_ray(clip_space_sample);

            // Get the radiance of this sample
            Spectrum radiance = radiance_at(scene, ray);

            // Set the pixels affected by the sample
            add_sample(radiance, sample, dimensions, offset, weights);
        }

        // Rescale pixels of the tile
        scale_samples(offset, weights);
    }

    void Integrator::add_sample(Spectrum radiance, FloatVector2 sample, FloatVector2 dimensions,
                                ImageVector2 offset, weights_t& weights)
    {
        const FloatScalar filter_center_x = (sample.x * dimensions.x) - 0.5;
        const FloatScalar filter_center_y = (sample.y * dimensions.y) - 0.5;

        // Get the top left (x1y1) and bottom right (x2y2) pixels affected by the filter
        const size_t pixel_x1 = static_cast<size_t>(
            std::max<FloatScalar>(std::ceil(filter_center_x - _filter.radius()), 0.0));
        const size_t pixel_y1 = static_cast<size_t>(
            std::max<FloatScalar>(std::ceil(filter_center_y - _filter.radius()), 0.0));
        const size_t pixel_x2 = static_cast<size_t>(std::clamp<FloatScalar>(
            std::floor(filter_center_x + _filter.radius()) + 1.0, 0.0, dimensions.x));
        const size_t pixel_y2 = static_cast<size_t>(std::clamp<FloatScalar>(
            std::floor(filter_center_y + _filter.radius()) + 1.0, 0.0, dimensions.y));

        // For each affected pixel, modify the film value and weight using the filter
        for (size_t x = pixel_x1; x < pixel_x2; x++)
        {
            for (size_t y = pixel_y1; y < pixel_y2; y++)
            {
                const FloatScalar weight =
                    _filter.get_weight({x - filter_center_x, y - filter_center_y});
                weights.set({x, y}, *(weights.get({x, y})) + weight);

                const std::array<size_t, 2> coords{offset.x + x, offset.y + y};
                _film.set(coords, *(_film.get(coords)) + radiance * weight);
            }
        }
    }

    void Integrator::scale_samples(ImageVector2 offset, weights_t& weights)
    {
        const size_t w = weights.shape()[0];
        const size_t h = weights.shape()[1];

        for (size_t x = 0; x < w; x++)
        {
            for (size_t y = 0; y < h; y++)
            {
                const FloatScalar weight = *(weights.get({x, y}));
                if (weight > 0)
                {
                    const std::array<size_t, 2> coords{offset.x + x, offset.y + y};
                    _film.set(coords, *_film.get(coords) / weight);
                }
            }
        }
    }

    Spectrum Integrator::radiance_at(const Scene& scene, const FloatRay& ray)
    {
        const auto atmosphere_intersection =
            scene.planet()->intersect_ray_with_outer_atmosphere(ray);

        // The ray has not hit anything: return the color of the universe (it's black)
        if (!atmosphere_intersection)
            return Spectrum::black();

        const FloatVector p_a = atmosphere_intersection->location;

        const auto ground_intersection = scene.planet()->intersect_ray_with_ground(ray);
        const FloatVector p_b = ground_intersection ? ground_intersection->location
                                                    : atmosphere_intersection->back_location;
        Spectrum radiance = Spectrum::black();

        // Compute the atmospheric scattering
        if ((p_a - p_b).norm() > INTERSECTION_BIAS)
            radiance += get_atmospheric_scattering(p_a, p_b, scene, ray);

        // If we have a collision with the ground, take it into account
        if (ground_intersection)
            radiance += get_attenuated_ground_radiance(
                p_a, p_b,
                scene); // TODO: reuse the t(p_a,p_b) computed from the atmospheric scattering

        return radiance;
    }

    Spectrum Integrator::get_attenuated_ground_radiance(const FloatVector& p_a,
                                                        const FloatVector& p_b, const Scene& scene)
    {
        const FloatVector light_vector = -scene.sun_direction();
        const FloatVector normal = (p_b - scene.planet()->location()).normalize();
        const FloatScalar cos_theta = light_vector.dot(normal);

        // Get the sun ray path to know how much light has been attenuated before reaching the
        // ground
        const auto sun_ray_intersection =
            scene.planet()->intersect_ray_with_outer_atmosphere(FloatRay{p_b, light_vector});

        if (!sun_ray_intersection)
            return Spectrum::black(); // This is not supposed to happen

        // Attenuate the sun radiance
        const FloatVector p_c = sun_ray_intersection->location;
        const auto [t_pb_pc_rayleigh, t_p_pc_mie] = t(p_b, p_c, scene);
        const Spectrum attenuated_sun_irradiance =
            scene.sun_irradiance() * t_pb_pc_rayleigh.minus_exp() * t_p_pc_mie.minus_exp();

        // Compute the radiance of the ground without attenuation
        const Spectrum ground_radiance =
            scene.planet()->brdf() * attenuated_sun_irradiance * cos_theta;

        // Attenuate the ground radiance
        const auto [t_pa_pb_rayleigh, t_pa_pb_mie] = t(p_a, p_b, scene);
        return ground_radiance * (t_pa_pb_rayleigh + t_pa_pb_mie).minus_exp();
    }

    std::pair<Spectrum, Spectrum> Integrator::t(const FloatVector& p_1, const FloatVector& p_2,
                                                const Scene& scene)
    {
        // Here, we will cast rays all the way between the two points to approximate the value of
        // t(p_1, p_2) Since we are approximating an integral (the integral of exp(-h/H_0))), we
        // have to multiply by the depth of the ray to have correct values.
        //
        // To avoid doing the computation twice, we are doing both Rayleigh and Mie scattering. The
        // values that change are beta and H_0, which are defined as constants.
        //
        // The integrated values are FloatScalar and not Spectrum because beta does not depend of
        // the integration variable. This allows us to move it outside the integral and thus
        // eliminate any dependency to the wavelength inside the integral.

        FloatScalar rayleigh_attenuation = 0.0;
        FloatScalar mie_attenuation = 0.0;

        if (!integrate_between(p_1, p_2, scene, [&](FloatVector p, FloatScalar d) {
                const FloatScalar h = scene.planet()->distance_to_ground(p);

                const FloatScalar rho_rayleigh = std::exp(-h / RAYLEIGH_H0);
                const FloatScalar rho_mie = std::exp(-h / MIE_H0);

                rayleigh_attenuation += d * rho_rayleigh;
                mie_attenuation += d * rho_mie;
            }))
            return {Spectrum::white(), Spectrum::white()};
        else
            return {RAYLEIGH_BETA * rayleigh_attenuation, MIE_BETA * mie_attenuation};
    }

    Spectrum Integrator::get_atmospheric_scattering(const FloatVector& p_a, const FloatVector& p_b,
                                                    const Scene& scene, const FloatRay& view_ray)
    {
        // Similarly to the function t, we will integrate between two points.
        // However, this function is more complex, because we have to compute more values.

        // This value is just used to check that we are correctly integrating over the whole
        // distance. In release profile, it will be optimized out.
        [[maybe_unused]] double total_distance = 0.0;

        // The scattering we are trying to compute
        Spectrum rayleigh_scattering = Spectrum::black();
        Spectrum mie_scattering = Spectrum::black();

        // This value is constant through the integral
        const FloatVector light_vector = -scene.sun_direction();

        if (!integrate_between(p_a, p_b, scene, [&](FloatVector p, FloatScalar d) {
                total_distance += d;

                const FloatScalar h = scene.planet()->distance_to_ground(p);

                // Ray from p in sun direction
                const FloatVector normal = (p - scene.planet()->location()).normalize();
                const FloatVector offset_origin =
                    p + normal * INTERSECTION_BIAS; // Offset to avoid self-shadowing

                const FloatRay ray_to_sun = FloatRay{offset_origin, light_vector};

                // We have to check if the sun is not occluded by the planet
                if (!scene.planet()->is_ray_shadowed(ray_to_sun))
                {
                    FloatVector p_c =
                        scene.planet()->intersect_ray_with_outer_atmosphere(ray_to_sun)->location;

                    // For both rayleigh an mie, the values of t(p, p_a) and t(p, p_c)
                    const auto [t_p_pc_rayleigh, t_p_pc_mie] = t(p, p_c, scene);
                    const auto [t_p_pa_rayleigh, t_p_pa_mie] = t(p, p_a, scene);

                    const FloatScalar rho_rayleigh = std::exp(-h / RAYLEIGH_H0);
                    const FloatScalar rho_mie = std::exp(-h / MIE_H0);

                    rayleigh_scattering +=
                        (t_p_pc_rayleigh + t_p_pa_rayleigh).minus_exp() * rho_rayleigh * d;
                    mie_scattering += (t_p_pc_mie + t_p_pa_mie).minus_exp() * rho_mie * d;
                }
            }))
        {
            assert(false);
        }

#ifndef NDEBUG
        const double expected_distance = (p_a - p_b).norm();
        if (std::abs(total_distance - expected_distance) > 10 * INTERSECTION_BIAS)
        {
            std::cout << "Error: invalid integration " << total_distance << " instead of "
                      << expected_distance << " (" << std::abs(total_distance - expected_distance)
                      << unit << " of difference)\n";
        }
#endif

        // Compute the scattered light from integrated values
        const FloatScalar cos_theta = scene.sun_direction().dot(view_ray.direction);

        const FloatScalar Fr_rayleigh = 0.75 * (1.0 + (cos_theta * cos_theta));

        const FloatScalar u = 0.75; // Should vary with atmospheric conditions and wavelength
        const FloatScalar u2 = u * u;
        const FloatScalar u3 = u2 * u;
        const FloatScalar u4 = u3 * u;
        const FloatScalar x = (u * 5.0 / 9.0) + (u3 * 125.0 / 729.0)
            + std::sqrt((64.0 / 27.0) - (u2 * 325.0 / 243.0) + (u4 * 1250.0 / 2187.0));
        const FloatScalar g = (u * 5.0 / 9.0)
            - ((4.0 / 3.0) - (u2 * 25.0 / 81.0)) * std::pow(x, -1.0 / 3.0) + std::pow(x, 1.0 / 3.0);
        const FloatScalar Fr_mie_num = (3.0 * (1.0 - (g * g))) * (1.0 + (cos_theta * cos_theta));
        const FloatScalar Fr_mie_denom =
            (2.0 * (2.0 + g * g)) * std::pow(1.0 + g * g - 2 * g * cos_theta, 1.5);
        const FloatScalar Fr_mie = Fr_mie_num / Fr_mie_denom;

        const Spectrum scattering = scene.sun_irradiance()
            * (RAYLEIGH_BETA * rayleigh_scattering * Fr_rayleigh
               + MIE_BETA * mie_scattering * Fr_mie)
            / (4.0 * M_PI);

        return scattering;
    }

    bool Integrator::integrate_between(const FloatVector& p_1, const FloatVector& p_2,
                                       const Scene& scene,
                                       std::function<void(FloatVector p, FloatScalar d)> f) const
    {
        // Here, we will cast rays all the way between the two points to approximate the value of
        // the integral. We are doing it by computing discrete steps of it that has to be scaled
        // by the distance d to be correct.
        //
        // We also have to update the limit at each iteration to not overflow behind p_2.

        if ((p_1 - p_2).norm() <= INTERSECTION_BIAS)
            return false;

        // Here, we are integrating between p_a and p_b, the same way we are doing in t
        FloatRay ray = FloatRay::get_between(p_1, p_2);
        ray.limit += INTERSECTION_BIAS;

        std::optional<Intersection> intersection;
        bool at_least_one_step = false;
        while ((intersection = scene.planet()->intersect_ray_with_atmosphere_layers(ray)))
        {
            // We offset d here to avoid hitting the same surface at next iteration
            const FloatScalar d = intersection->depth + INTERSECTION_BIAS;
            const FloatVector p = intersection->location;

            f(p, d);

            ray.origin = ray.point_at(d);
            ray.limit -= d;
            at_least_one_step = true;
        }

        return at_least_one_step;
    }
} // namespace as
