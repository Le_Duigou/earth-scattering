#include <iostream>
#include <memory>

#include "core/image/image.hh"
#include "core/types/fwd.hh"
#include "core/types/spectrum.hh"
#include "image_exporters/ppm_exporter/ppm_exporter.hh"
#include "integrator/integrator.hh"
#include "options/options.hh"
#include "planet/planet.hh"
#include "scene/scene.hh"

int main(int argc, char** argv)
{
    const as::Options options{argc, argv};

    if (options.error)
        return 1;

    as::Integrator::film_t film{options.film_dimension, as::Spectrum::black()};
    as::Integrator integrator{film, options.tile_size, options.samples, options.filter_radius};

    integrator(*options.scene);

    auto exporter = as::PpmImageExporter("result.ppm");
    if (!exporter.export_image(film))
        return 2;

    return 0;
}
