#include <gtest/gtest.h>

#define TEST_SECTION Ray

#include "core/types/fwd.hh"

TEST(TEST_SECTION, sphere_normal_hit)
{
    const auto [d1, d2] =
        as::FloatRay{{-2, 0, 0}, {1, 0, 0}}.intersection_with_sphere({0.0, 0.0, 0.0}, 1.0);

    ASSERT_NEAR(*d1, 1.0, 1.0e-7);
    ASSERT_NEAR(*d2, 3.0, 1.0e-7);
}

TEST(TEST_SECTION, sphere_normal_no_hit_limit)
{
    const auto [d1, d2] =
        as::FloatRay{{-2, 0, 0}, {1, 0, 0}, 0.5}.intersection_with_sphere({0.0, 0.0, 0.0}, 1.0);

    ASSERT_FALSE(d1);
    ASSERT_FALSE(d2);
}

TEST(TEST_SECTION, sphere_normal_half_hit_limit)
{
    const auto [d1, d2] =
        as::FloatRay{{-2, 0, 0}, {1, 0, 0}, 2.0}.intersection_with_sphere({0.0, 0.0, 0.0}, 1.0);

    ASSERT_NEAR(*d1, 1.0, 1.0e-7);
    ASSERT_FALSE(d2);
}

TEST(TEST_SECTION, sphere_normal_hit_limit)
{
    const auto [d1, d2] =
        as::FloatRay{{-2, 0, 0}, {1, 0, 0}, 3.0}.intersection_with_sphere({0.0, 0.0, 0.0}, 1.0);

    ASSERT_NEAR(*d1, 1.0, 1.0e-7);
    ASSERT_NEAR(*d2, 3.0, 1.0e-7);
}

TEST(TEST_SECTION, sphere_normal_no_hit)
{
    const auto [d1, d2] =
        as::FloatRay{{-2, 0, 0}, {0, 1, 0}}.intersection_with_sphere({0.0, 0.0, 0.0}, 1.0);

    ASSERT_FALSE(d1);
    ASSERT_FALSE(d2);
}

TEST(TEST_SECTION, sphere_inside_hit)
{
    const auto [d1, d2] =
        as::FloatRay{{0, 0, 0}, {0, 1, 0}}.intersection_with_sphere({0.0, 0.0, 0.0}, 4.0);

    ASSERT_NEAR(*d1, 4.0, 1.0e-7);
    ASSERT_FALSE(d2);
}

TEST(TEST_SECTION, sphere_inside_hit_2)
{
    const auto [d1, d2] =
        as::FloatRay{{0, 2, 0}, {0, 1, 0}}.intersection_with_sphere({0.0, 0.0, 0.0}, 4.0);

    ASSERT_NEAR(*d1, 2.0, 1.0e-7);
    ASSERT_FALSE(d2);
}

TEST(TEST_SECTION, sphere_inside_hit_limit)
{
    const auto [d1, d2] =
        as::FloatRay{{0, 1.0, 0}, {0, 1, 0}, 4.0}.intersection_with_sphere({0.0, 0.0, 0.0}, 4.0);

    ASSERT_NEAR(*d1, 3.0, 1.0e-7);
    ASSERT_FALSE(d2);
}

TEST(TEST_SECTION, sphere_inside_no_hit_limit)
{
    const auto [d1, d2] =
        as::FloatRay{{0, 1.0, 0}, {0, 1, 0}, 0.5}.intersection_with_sphere({0.0, 0.0, 0.0}, 4.0);

    ASSERT_FALSE(d1);
    ASSERT_FALSE(d2);
}
TEST(TEST_SECTION, point_at)
{
    const as::FloatRay ray = as::FloatRay{{1.0, 1.0, 0.0}, {0.0, 1.0, 0.0}};
    const as::FloatVector result = ray.point_at(2.0);

    ASSERT_EQ(result, (as::FloatVector{1.0, 3.0, 0.0}));
}
