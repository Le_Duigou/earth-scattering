# Atmospheric scattering
This project aims to implement simply the scattering effect of the Earth atmosphere. It is based on the paper of Tomoyuki Nishita "Display of The Earth Taking into Account Atmospheric Scattering".

You can find results in the ``results`` directory. These took in total 7m 28s with 15 atmosphere layers, 8 sample per pixels and dimensions of 1024x1024.

## Building the project
To build this project with ``cmake``:

```sh
mkdir build && cd build
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Release ..
make
./atmospheric-scattering
```

There are two build types, release and debug. Debug build are more verbose and contains more checks (no null ray, valid integration, etc.) and release builds have better optimization flags to speed the execution (-O3 -march=native).

If gTests is detected, a simple test suite will also be built, although it is minimalist. This is not a dependency however, as the project does not require it to build.

## Usage
```
Usage: ./atmospheric-scattering [--dimensions width height] [--samples count] [--tile-size size] [--filter-radius radius] [--layers count] [--scene-id id]
```

For instance:
```sh
./atmospheric-scattering --dimensions 512 512 --layers 15 --samples 4 --filter-radius 1.0 --scene-id 3
```

There are six scene, from number 0 to 5 included. The images will be saved as "result.ppm".

## Architecture

### Core
This folder contains all kind of classes that are independent of the rest of the program but used everywhere else, such as vectors, colors or images.

### Camera
The camera, a simple pinhole perspective camera located in ``camera/camera.hh/.cc`` is used to convert between clip space to world space. It is necessary to create rays.

### Sampler/Filter
These classes are very small (which is, to my eyes, a great quality) and are used to sample pixels to render and properly apply the result into a film. In a real program, these would be abstract class, with different sampling/filtering strategies implementing them.

### Scene
The scene (``scene/scene.hh/.cc``) is a class containing a single planet, the sun data and the camera.

### Options
The option parser in ``options/options.hh/.cc`` is used to parse command line arguments and initialize values correctly. It is the class creating the Scene from the given id.

### Planet
Representation of a planet is done in ``planet/planet.hh/.cc``. What is worth noticing here is that we compute different radius for layers of the atmosphere. These will be used as steps for integration. The number of layer can be change via a command line argument.

There are three collision detection functions, one for the outer atmosphere, one for the ground and one for the atmosphere layers. The one for layers does not check every layer, but only the one with radius closes to the distance between the ray origin and the center. This allows us to skip lots of tests and I noticed visible time improvements with it.

### Integrator
Most of the code you are certainly interested in is located in ``integrator/integrator.hh/.cc``. The integrator class has its name because it tries to solve the rendering equation, but it also has to handle the integration for the atmospheric scattering and attenuation.

The first part of the integrator split the film to render on into tiles, samples point in it, get a ray corresponding to the sample, call the function get get the radiance corresponding to this ray and apply it to the film directly using a filter (simple triangle filter in this case). The assumption here is that the whole program is mono-threaded, therefore, no proper isolation work has been done to avoid data races. Globally, this part is rough and just here to have a minimal working base for the real important part starting in the ``radiance_at`` function.

In this function, we first check an intersection with the outer atmosphere. If we do not intersect it, it means we are seeing the space and we can leave early.

The collision function allows us to get the back point, so we have p_a (the entering point) and a candidate for p_b. However, we have to check for a collision to the ground to know the real value of p_b.

Once we have them, we have to compute the scattered light between these two points. This is done in ``get_atmospheric_scattering``.

Since we have two functions integrating between points with, as steps, the layers of the atmosphere, code is factored using the integrate_between function which takes a lambda as an argument. The idea is to cast a ray from p_a to p_b, ray that has a limit (intersections after the limit will be ignored). Each step, we intersect the ray with the layers, call the lambda, move the ray origin to the intersection point and reduce the limit by the distance traveled. Whatever happens in the lambda function, the result should be scaled by the distance, otherwise the results will be invalid.

For ``get_atmospheric_scattering``, at each step we compute the location of p_c (point of sun light entry in the atmosphere), the values of rho and the values of t(p, p_a)/t(p, p_c), for both rayleigh and mie, and solve the variable part of the equation. The rest of the equation (Is * Fr * beta / 4 PI) is constant and can thus be computed once outside the iterations.

To avoid depending on lambda and K, we use directly the beta values. We know that ``beta = 4 * PI * K / lambda^4``, therefore, the ``K / lambda^4`` can be substituted by ``beta / 4 * PI``. Values of beta can be found in ``constants/constants.hh``

The value of ``t(p, p_a/c)`` requires itself an integration, but is much simpler. That's why I recommend to look at this function first.

A possible optimization would be to compute "on the fly "t(p, p_a)" as we are already traveling from p_a, thus avoiding a full integration.

Once we get the light scattered by the atmosphere, we have to get the radiance of the planet (if collision with ground).
In this version of the program, the planet is very simple with only a lambert diffuse component with a constant color for the whole planet (it is a grey around (0.2, 0.2, 0.2) to be light enough for shading to be visible but dark enough to not take too much place in the final render). Possible amelioration in the future could include proper brdf handling, textures, etc.

However, the radiance needs itself to be attenuated, therefore we also have to compute both the attenuation between p_b and p_a (which could be improved by reusing a value computed with the atmospheric scattering) and the attenuation of the light before it hit the ground. We can here reuse the t function.
